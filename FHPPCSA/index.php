<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language;?>" lang="<?php echo $this->language; ?>"
>
    <head>
        <jdoc:include type="head" /></head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto:300,400,400i,700" rel="stylesheet">      
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/jquery-ui.structure.min.css">
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/jquery-ui.min.css">
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/fhppcsa.css">
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
        <?php 
            $lang = JFactory::getLanguage();
            $active = JFactory::getApplication()->getMenu()->getActive();
            $languages = JLanguageHelper::getLanguages('lang_code');
            $languageCode = $languages[ $lang->getTag() ]->sef;
            //echo '<pre>'.print_r($languageCode, true).'</pre>'; 
        ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class="topo fullwidth desktop">
            <div class="maxWidth">
                <div class="options">
                    <div class="position">
                        <div class="langs">
                            <jdoc:include type="modules" name="flags"/>
                        </div>
                        <div class="mail"><i></i></div>
                        <div class="searchbt"><i></i></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                 <?php if($this->countModules('search')) : ?>
                     <div class="searchbar">
                         <jdoc:include type="modules" name="search"/>
                     </div>
                     <div class="clearfix"></div>
                <?php endif; ?>
                <div class="menu"><jdoc:include type="modules" name="menuTopo"/></div>
            </div>
        </div>
        <div class="topo fullwidth mobile">
                <div class="cabecalho">
                    <div class="btnmenu"><i></i></div>
                    <div class="btnpesquisa"><i></i></div>
                    <div class="hiddCont">
                        <div class="menu"><jdoc:include type="modules" name="menuTopo"/></div>
                        <div class="options"> 
                            <div class="langs">
                                <jdoc:include type="modules" name="flags"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php if($this->countModules('search')) : ?>
                         <div class="searchbar">
                             <jdoc:include type="modules" name="search"/>
                         </div>
                         <div class="clearfix"></div>
                    <?php endif; ?>
                </div>
                
        </div>
        <?php if($this->countModules('homeBanner') || $this->countModules('internalBanner')|| $this->countModules('mobileBanner')) : ?>
                 <?php $extra = '';?>

                 <?php if($this->countModules('internalBanner')) : ?>
                    <?php $extra = 'smallBanner';?>
                <?php endif; ?>
                <div class="fullwidth banner <?php echo $extra;?>" style="max-width:100%; overflow:hidden">
                <div class="degrade"></div>
                <?php if($this->countModules('mobileBanner')) : ?>
                    <div class="mobile"><jdoc:include type="modules"  name="mobileBanner"/></div>
                <?php endif; ?>
                <?php if($this->countModules('homeBanner')) : ?>
                    <div class="desktop"><jdoc:include type="modules" name="homeBanner"/></div>
                <?php endif; ?>
                 <?php if($this->countModules('internalBanner')) : ?>
                   <div class="desktop"> <jdoc:include type="modules" name="internalBanner"/></div>
                <?php endif; ?>
                 <div class="overBanner">
                    <div class="logo"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/img/logo.png" alt="logotipo" title="logotipo"/></div>
                    <div class="texto">Federação Hispano-Portuguesa de pedagogia<br/> curativa e socioterapia antroposófica</div>
                </div>
            </div>
        <?php endif; ?>
        <div class="content fullwidth purple">
            <div class="maxWidth">
                 <?php if($this->countModules('purpleArea')) : ?>
                    <jdoc:include type="modules" name="purpleArea"/>
                <?php endif; ?>
            </div>
        </div>
        <?php if($this->countModules('whiteArea')) : ?>
            <div class="content fullwidth white">
                <div class="maxWidth">
                        <jdoc:include type="modules" name="whiteArea"/>
                </div>
            </div>
        <?php endif; ?>
        <div class="content fullwidth white">
            <div class="maxWidth">
                <jdoc:include type="component" style="xhtml" />
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if($this->countModules('partners')) : ?>
            <div class="fullwidth partners">
                <div class="maxWidth">
                    <h1 class="title"><?php echo JText::_('TPL_PARCEIROS'); ?></h1>
                    <jdoc:include type="modules" name="partners"/>
                </div>
            </div>
        <?php endif; ?>
        <div class="fullwidth footer">
            <div class="maxWidth">
                <div class="Morada"><jdoc:include type="modules" name="morada"/></div>
                <div class="menuSec"><jdoc:include type="modules" name="menuSec"/></div>
                <div class="subscrever"><jdoc:include type="modules" name="subscrever"/>
                    <div class="title"><?php echo JText::_('TPL_SUBSCREVA'); ?></div>
                    <div class="form">
                        <input type="text" class="email" id="email" placeholder="<?php echo JText::_('TPL_SEU_MAIL'); ?>" />
                        <div class="btn" id="btNewsletter"></div>
                    </div>
                </div>
                <div class="creditos"><jdoc:include type="modules" name="creditos"/></div>
            </div>
        </div>
       
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/vendor/jquery-1.11.3.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/plugins.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/vendor/jquery-ui.min.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/vendor/jquery.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','<?php echo $this->params->get('googleAnalitycs');?>','auto');ga('send','pageview');
        </script>
        <script>
            $(document).ready(function(){
                $('.btnmenu i').click(function(){
                    $('.hiddCont').slideToggle();
                    if(!$(this).hasClass('close')){
                        $(this).addClass('close');
                    } else {
                        $(this).removeClass('close');
                    }
                });
                $('.searchbt').click(function(){
                    $('.searchbar').slideToggle();
                    $('.search-query').focus();
                })
                $('.btnpesquisa').click(function(){
                    $('.searchbar').slideToggle();
                    $('.search-query').focus();
                })
            });
        </script>

    </body>
</html>
