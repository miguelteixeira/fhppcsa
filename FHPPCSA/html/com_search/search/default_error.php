<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->error) : ?>
<div class="error">
			<?php echo $this->escape($this->error); ?>
</div>
<?php else: ?>
	<div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
		<?php if (!empty($this->searchword)):?>
			<p><?php echo ' 0 '. JText::_('TPL_RESULTADOS_ENCONTRADOS_PARA_A_PALAVRA'). ' '.$this->searchword; ?>
		<?php endif;?>
	</div>
<?php endif; ?>
