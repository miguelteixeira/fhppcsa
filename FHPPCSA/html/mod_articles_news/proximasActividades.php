<?php
/**
 * @package     fhppcsa
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
	<h1 class="titleSec"><?php echo JText::_('TPL_PROXIMAS_ACTIVIDADES');?></h1>
	<?php foreach ($list as $item) : ?>
		<?php if (strtotime(date('Y-m-d',strtotime($item->created))) >= strtotime(date('Y-m-d'))) : ?>
			<?php
				$images = json_decode($item->images); 
				$monthLabel = 'TPL_MES'.date('n', strtotime($item->created));
			?>
			<div class="ItemNextActivitie">
				<div class="desktop">
					<div class="date">
						<div class="dia"><?php echo date('d',strtotime($item->created));?></div>
						<div class="mes"><?php echo JText::_($monthLabel);?></div>
						<div class="ano"><?php echo date('Y',strtotime($item->created));?></div>
					</div>
					<div class="image"><img src="<?php echo $images->image_intro; ?>"/></div>
					<div class="description"><b><?php echo $item->title?></b><br/><?php echo $item->introtext?></div>
					<div class="clearfix"></div>
				</div>
				<div class="mobile">
					
					<div class="image"><img src="<?php echo $images->image_intro; ?>"/></div>
					<div class="dat">
						<div class="date">
						<div class="dia"><?php echo date('d',strtotime($item->created));?></div>
						<div class="mes"><?php echo JText::_($monthLabel);?></div>
						<div class="ano"><?php echo date('Y',strtotime($item->created));?></div>
					</div>
					</div>
					<div class="txt">
						<div class="description"><b><?php echo $item->title?></b><br/><?php echo $item->introtext?></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		<?php endif; ?>
	<?php endforeach; ?>
</div>