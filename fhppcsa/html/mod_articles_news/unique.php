<?php
/**
 * @package     SAMASA
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
	<?php foreach ($list as $item) : ?>
		<div class="promListItem">
			<?php
				$images = json_decode($item->images);
				$urls = json_decode($item->urls);
			?>
			<img src="<?php echo $images->image_intro?>" alt="<?php echo $images->image_intro_alt;?>">
			<a href="<?php echo $urls->urla;?>"><div class="btnOver"><?php echo JText::_('TPL_KNOW_MORE'); ?></div></a>
		</div>
	<?php endforeach; ?>
</div>
