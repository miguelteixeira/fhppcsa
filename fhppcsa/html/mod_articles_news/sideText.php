<?php
/**
 * @package     SAMASA
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php foreach ($list as $item) : ?>
	<?php
		$images = json_decode($item->images);
		$urls = json_decode($item->urls);
	?>
	<a href="<?php echo $urls->urla;?>">
		<div class="noticiaSpot">
	        <img src="<?php echo $images->image_intro?>" alt="<?php echo $images->image_intro_alt?>">
	        <div class="textoNoticia"><?php echo $item->title?></div>
	    </div>
	</a>
<?php endforeach; ?>
