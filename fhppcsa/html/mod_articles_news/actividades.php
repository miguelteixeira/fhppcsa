<?php
/**
 * @package     fhppcsa
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
	<?php foreach ($list as $item) : ?>
		<?php
			$images = json_decode($item->images);
		?>
		<?php 
			//echo '<pre>'.print_r($images, true).'</pre>';
			$monthLabel = 'TPL_MES'.date('n', strtotime($item->created));
			//echo $monthLabel;
		?>
		<div class="actividadeItem">
			<div class="image">
				<img src="<?php echo $images->image_intro; ?>"/>
			</div>
			<div class="title"><?php echo $item->title?></div>
			<div class="data">
				<?php 
					echo date('j ', strtotime($item->created));
					echo JText::_($monthLabel);
					echo date(' Y', strtotime($item->created)); 
				?>
					
				</div>
			<!--<div class="titleDesc"><?php echo $item->title?></div>-->
			<div class="decricao"><?php echo $item->introtext?></div>
		</div>
	<?php endforeach; ?>
</div>