<?php
/**
 * @package     fhppcsa
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="newsflash<?php echo $moduleclass_sfx; ?>">
	<?php foreach ($list as $item) : ?>
		<div class="Item">
			<?php if (strpos(strtolower($item->title), 'notitle') === false) : ?>
				<h1><?php echo $item->title?></h1>
			<?php endif; ?>
			<?php echo $item->introtext?>
				
		</div>
	<?php endforeach; ?>
</div>